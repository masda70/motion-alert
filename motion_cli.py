#!/usr/bin/env python
__author__ = 'dmontoya'

import config_logging
import argparse
import logging
import os
from upload import upload
from sms import send_sms
from alarm import alarm

if __name__ == '__main__':
    previous_chdir = os.getcwd()
    try:
        chdir_name = os.path.dirname(__file__)
        # if __file__ is inside current working directory chdir_name will be empty
        if chdir_name:
            os.chdir(chdir_name)
        config_logging.setup()
        logger = logging.getLogger(__name__)
        parser = argparse.ArgumentParser(description='Motion Alert')

        parser.add_argument('-u', '--upload')
        parser.add_argument('-a', '--alert', action='store_true')
        parser.add_argument('-s', '--start', action='store_true')
        parser.add_argument('-e', '--end', action='store_true')
        parser.add_argument('-t', '--setup', action='store_true')
        parser.add_argument('-d', '--detected', action='store_true')

        args = parser.parse_args()
        if args.detected:
            logger.info('Area detected.')
        if args.setup:
            upload.setup()
        if args.upload is not None:
            upload.upload_file(args.upload)
        if args.alert:
            send_sms.send_sms_alerts()
        if args.start:
            logger.info('Event start.')
            send_sms.send_sms_alerts()
            alarm.start_alarm()
        if args.end:
            logger.info('Event end.')
            alarm.end_alarm()
    finally:
        os.chdir(previous_chdir)