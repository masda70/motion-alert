from nexmomessage import NexmoMessage
import logging
import config

TELEPHONE_NUMBERS_PATH = 'telephone_numbers.txt'

logger = logging.getLogger(__name__)


def send_sms_alert(number):
    try:
        logger.info('Sending SMS alert to %s.', number)
        msg = {
            'reqtype': 'json',
            'api_key': config.NEXMO_API_KEY,
            'api_secret': config.NEXMO_API_SECRET,
            'from': 'MASDA70',
            'to': number,
            'text': 'Intrusion Alert: https://m70.no-ip.org'
        }
        sms = NexmoMessage(msg)
        sms.set_text_info(msg['text'])

        response = sms.send_request()

        if response:
            logger.info(response)
        else:
            logger.error('error occurred sending SMS to %s', number)
    except Exception, e:
        logger.error('error sending SMS alert %s', e)


def send_sms_alerts():
    logger.debug('Sending SMS alerts.')
    try:
        with open(TELEPHONE_NUMBERS_PATH) as f:
            numbers = f.read().splitlines()
        for number in numbers:
            send_sms_alert(number)
    except IOError, e:
        logger.error('IOError reading %s: %s', TELEPHONE_NUMBERS_PATH, e)
