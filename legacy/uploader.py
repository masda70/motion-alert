#!/usr/bin/env python
import os
import sys
from datetime import datetime
import config_logging
import logging


def upload_files(path):
    if not os.path.exists(path):
        return
    os.chdir(path)
    for files in os.listdir("."):
        if files.endswith(".tar"):
            cmd = "/home/motion/Dropbox-Uploader/dropbox_uploader.sh upload " + files + " " + files
            os.system(cmd)
            os.system("rm " + path + files)


def upload_file(file):
    if not(os.path.exists(file) and os.path.isfile(file)):
        return
    (directory, filename) = os.path.split(file)
    cmd =  "/home/motion/Dropbox-Uploader/dropbox_uploader.sh upload " + file + " " + filename    
    os.system(cmd)
    os.system("rm " + file)


if __name__ == '__main__':
    config_logging.setup()
    if len(sys.argv) == 1:
        upload_files("/home/motion/tmp/")
    elif len(sys.argv) == 2:
        upload_file(sys.argv[1])
    

