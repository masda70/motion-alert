import subprocess
import sys
import os
import logging

ALARM_FILE = "alarm/alarm.mp3"

logger = logging.getLogger(__name__)

def start_alarm():
    alarm_file = os.path.abspath(ALARM_FILE)
    logger.debug('Starting alarm %s', alarm_file)
    pid = subprocess.Popen(["mpg123", "--loop", "-1", alarm_file])


def end_alarm():
    os.system("pkill mpg123")