#!/bin/bash
HOME=$(eval echo ~${SUDO_USER})
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $HOME/python/bin/activate
mkdir -p "$DIR/logs"
exec >> "$DIR/logs/motion-cli.log"
exec 2>&1
python $DIR/motion_cli.py "$@"