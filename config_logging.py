import logging
import config


def setup():
    logfile = 'logs/motion_alert.log'
    if config.DEBUG:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(format='%(asctime)s %(message)s', filename=logfile, level=level)