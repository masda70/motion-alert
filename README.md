# motion-alert #

*motion-alert* is a Python application that interfaces with the popular *motion* application to automatically send SMS and trigger sound alerts while uploading camera shots during motion detection events.

## Dependencies ##

- Python >= 2.7 (installed through virtualenv and assumed to be installed at `$HOME/python`)
- mpg123
- motion

## Installation ##

Installing dependencies:

`pip install -r requirements.txt`

`apt-get install mpg123`

`motion` should be installed and properly configured for your hardware (camera).

## Setup ##

Setup cronjob to execute every minute:

`* * * * * /path/to/motion-alert/motion_cron.sh`

Create file `telephone_numbers.txt` with a list of telephone numbers in international format (00), one per line:

```
0015417543010
00498963648018
```

Setup motion.conf with the following lines

```
# Command to be executed when an event starts. (default: none)
# An event starts at first motion detected after a period of no motion defined by event_gap
on_event_start /path/to/motion-alert/motion-cli.sh -s

# Command to be executed when an event ends after a period of no motion
# (default: none). The period of no motion is defined by option event_gap.
on_event_end /path/to/motion-alert/motion-cli.sh -e

# Command to be executed when a picture (.ppm|.jpg) is saved (default: none)
# To give the filename as an argument to a command append it with %f
on_picture_save /path/to/motion-alert/motion-cli.sh -u %f

# Command to be executed when motion in a predefined area is detected
# Check option 'area_detect'.   (default: none)
on_area_detected /path/to/motion-alert/motion-cli.sh -d
```

Setup Dropbox authorization using:

`python motion_cli.py -t`

## TODO ##

Fill this todo list

## History ##

v0.1.0 (11-Feb-2015):

1) sms alerts
2) sound alert
3) dropbox upload

## License ##

Licensing conditions (MIT) can be found in LICENSE.txt.