import config
import logging
import os
from dropbox.client import DropboxOAuth2FlowNoRedirect, DropboxClient
from dropbox import rest as dbrest

logger = logging.getLogger(__name__)

TOKEN_FILE_PATH = 'tokens/dropbox_token.txt'


def write_token(access_token):
    try:
        logging.debug('Saving Dropbox access token...')
        (directory, _) = os.path.split(TOKEN_FILE_PATH)
        os.makedirs(directory)
        with open(TOKEN_FILE_PATH, 'w') as token_file:
            token_file.write(access_token)
            logging.debug('Dropbox access token saved!')
    except IOError, e:
        logging.error('could not save Dropbox access token: %s', e.message)
        raise e


def read_token():
    try:
        with open(TOKEN_FILE_PATH, 'r') as token_file:
            return token_file.read()
    except IOError, e:
        logging.error('could not retrieve Dropbox access token: %s', e.message)
        raise e


def setup():
    # Get your app key and secret from the Dropbox developer website
    app_key = config.DROPBOX_APP_KEY
    app_secret = config.DROPBOX_APP_SECRET

    flow = DropboxOAuth2FlowNoRedirect(app_key, app_secret)

    # Have the user sign in and authorize this token
    authorize_url = flow.start()
    print '1. Go to: ' + authorize_url
    print '2. Click "Allow" (you might have to log in first)'
    print '3. Copy the authorization code.'
    code = raw_input("Enter the authorization code here: ").strip()

    # This will fail if the user enters an invalid authorization code
    try:
        access_token, user_id = flow.finish(code)

        client = DropboxClient(access_token)
        account_info = client.account_info()

        write_token(access_token)

        print 'linked account: ', account_info
        logging.info('Linked Dropbox Account %s', account_info)
    except dbrest.ErrorResponse, e:
        print('Error: %s' % (e,))
        logging.error('Error while retrieving token %s', e)
        return


def upload_file(file_to_upload, delete=True):
    logger.debug('Attempting to upload file %s', file_to_upload)
    if not(os.path.exists(file_to_upload) and os.path.isfile(file_to_upload)):
        logging.error('Found no file %s', file_to_upload)
        return
    (directory, filename) = os.path.split(file_to_upload)
    client = DropboxClient(read_token())
    try:
        with open(file_to_upload, 'rb') as f:
            client.put_file('/' + filename, f)
            logging.debug('Uploaded file %s', file_to_upload)
        if delete:
            os.remove(file_to_upload)
    except IOError, e:
        logging.error("IOError uploading file: %s", e)
    except dbrest.ErrorResponse, e:
        logging.error("Error uploading file: %s", e)